import { AfterViewInit, Component, Input, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { RoomCreatorPageComponent } from '../../room-creator-page/room-creator-page.component';
import { TranslateService } from '@ngx-translate/core';
import { RoomService } from '../../../../services/http/room.service';
import { Room } from '../../../../models/room';
import { WriteCommentComponent } from '../../../shared/write-comment/write-comment.component';

@Component({
  selector: 'app-room-description-settings',
  templateUrl: './room-description-settings.component.html',
  styleUrls: ['./room-description-settings.component.scss']
})
export class RoomDescriptionSettingsComponent implements AfterViewInit {

  @ViewChild(WriteCommentComponent) writeComment: WriteCommentComponent;
  @Input() editRoom: Room;

  constructor(public dialogRef: MatDialogRef<RoomCreatorPageComponent>,
              public translationService: TranslateService,
              protected roomService: RoomService) {
  }


  ngAfterViewInit() {
    if (this.editRoom) {
      this.writeComment.commentData.currentData = this.editRoom.description;
    }
  }

  buildCloseDialogActionCallback(): () => void {
    return () => this.dialogRef.close('abort');
  }

  buildSaveActionCallback(): () => void {
    return () => this.save();
  }

  save(): void {
    this.editRoom.description = this.writeComment.commentData.currentData;
    this.roomService.updateRoom(this.editRoom).subscribe(r => this.editRoom = r);
    this.dialogRef.close('update');
  }

}
