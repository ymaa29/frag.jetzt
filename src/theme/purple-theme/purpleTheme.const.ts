export const purple = {

  '--primary': '#00324a',
  '--primary-variant': 'white',

  '--secondary': '#ff4500',
  '--secondary-variant': '#80d8ff',

  '--background': 'beige',
  '--surface': 'white',
  '--dialog': 'white',
  '--cancel': 'Firebrick',
  '--alt-surface': '#eeeeee',
  '--alt-dialog': '#eeeeee',

  '--on-primary': 'white',
  '--on-secondary': 'white',
  '--on-background': 'black',
  '--on-surface': 'black',
  '--on-dialog': 'black',
  '--on-cancel': '#ffffff',

  '--green': 'green',
  '--red': 'red',
  '--white': '#ffffff',
  '--yellow': 'red',
  '--blue': 'blue',
  '--purple': 'purple',
  '--magenta': '#ea0a8e',
  '--light-green': 'lightgreen',
  '--grey': 'slategrey',
  '--grey-light': 'lightgrey',
  '--black': 'black',
  '--moderator': 'lightsalmon',

};

export const purple_meta = {

  translation: {
    name: {
      en: 'Projector',
      de: 'Beamer',
    },
  },
  isDark: false,
  availableOnMobile: false,
  order: 1,
  scale_desktop: 1.5,
  scale_mobile: 1,
  previewColor: 'background',

};
